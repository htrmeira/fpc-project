#!/bin/bash

LOGS_DIR=logs/

find -name data.log -exec rm {} \;
mv *_test* /tmp/

for dire in 1000 10000 100000 1000000; do
	cd $LOGS_DIR/$dire"_data"
	pwd
	for teste in justice/*; do
		for i in $teste/*; do
			echo $i
			Rscript ../../media.R $i $teste/data.log
		done
	done

	for teste in nojustice/*; do
		for i in $teste/*; do
			echo $i
			Rscript ../../media.R $i $teste/data.log
		done
	done

	for i in "REENTRANT_LOCK_TEST" "SEMAPHORE_TEST" "REENTRANT_READ_WRITE_TEST" "SYNCHRONOUS_QUEUE_TEST" "ARRAY_BLOCKING_QUEUE_TEST" "ARRAY_BLOCKING_QUEUE_TEST_BLOCKING" "SYNCHRONIZED_TEST"; do
		python ../../merge.py $i `pwd`
	done

	Rscript ../../generate_graphic.R reentrant_lock_test reentrant_lock_test_$dire.png "ReentrantLock - $dire operações"
	Rscript ../../generate_graphic.R array_blocking_queue_test array_blocking_queue_test_$dire.png "ArrayBlockingQueue - $dire operações"
	Rscript ../../generate_graphic.R array_blocking_queue_test_blocking array_blocking_queue_test_blocking_$dire.png "ArrayBlockingQueue - $dire operações bloqueantes"
	Rscript ../../generate_graphic.R semaphore_test semaphore_test_$dire.png "Semaphore - $dire operações"
	Rscript ../../generate_graphic.R reentrant_read_write_test reentrant_read_write_test_$dire.png "ReentrantReadWriteLock - $dire operações"
	Rscript ../../generate_graphic.R synchronous_queue_test synchronous_queue_test_$dire.png "SynchronousQueue - $dire operações"
	Rscript ../../generate_graphic.R synchronized_test synchronized_teste_$dire.png "Synchronized - $dire operações"
	cd ../../
done

mkdir -p graphics
find -name *.png -exec cp {} graphics/ \;
