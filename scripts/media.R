args <- commandArgs(TRUE)
fpc_table <- read.table(args[1], header=FALSE, fill=TRUE, na.string='NA')
resulttest <- colMeans(fpc_table[5], na.rm=TRUE)
write(resulttest, file=args[2], ncolumns=1, append=TRUE, sep=" ")
