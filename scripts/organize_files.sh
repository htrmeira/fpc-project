#!/bin/bash

LOGS_DIR=logs/

mkdir -p $LOGS_DIR/1000_data/
mkdir -p $LOGS_DIR/10000_data/
mkdir -p $LOGS_DIR/100000_data/
mkdir -p $LOGS_DIR/1000000_data/

mv $LOGS_DIR/*1000000*.log $LOGS_DIR/1000000_data/
mv $LOGS_DIR/*100000*.log $LOGS_DIR/100000_data/
mv $LOGS_DIR/*10000*.log $LOGS_DIR/10000_data/
mv $LOGS_DIR/*1000*.log $LOGS_DIR/1000_data/

function create_subdirs() {
	mkdir -p REENTRANT_LOCK_TEST/
	mkdir -p SEMAPHORE_TEST/
	mkdir -p REENTRANT_READ_WRITE_TEST/
	mkdir -p SYNCHRONOUS_QUEUE_TEST/
	mkdir -p ARRAY_BLOCKING_QUEUE_TEST/
	mkdir -p ARRAY_BLOCKING_QUEUE_TEST_BLOCKING/
	mkdir -p SYNCHRONIZED_TEST/

	mv *-1-* REENTRANT_LOCK_TEST/
	mv *-2-* SEMAPHORE_TEST/
	mv *-3-* REENTRANT_READ_WRITE_TEST/
	mv *-4-* SYNCHRONOUS_QUEUE_TEST/
	mv *-5-* ARRAY_BLOCKING_QUEUE_TEST/
	mv *-6-* ARRAY_BLOCKING_QUEUE_TEST_BLOCKING/
	mv *-7-* SYNCHRONIZED_TEST/
}

function organize_files() {
	mkdir -p justice
	mkdir -p nojustice

	mv com_justica-* justice/
	mv sem_justica-* nojustice/

	cd justice/
	for i in *; do
		name=`echo $i | sed 's/1.log/01.log/g' | sed 's/2.log/02.log/g' | sed 's/3.log/03.log/g' | sed 's/4.log/04.log/g'| sed 's/5.log/05.log/g' | sed 's/6.log/06.log/g' | sed 's/7.log/07.log/g' | sed 's/8.log/08.log/g' | sed 's/9.log/09.log/g'`
		mv $i $name
	done

	create_subdirs;

	cd ../nojustice
	for i in *; do
		name=`echo $i | sed 's/--/-/g' | sed 's/1.log/01.log/g' | sed 's/2.log/02.log/g' | sed 's/3.log/03.log/g' | sed 's/4.log/04.log/g'| sed 's/5.log/05.log/g' | sed 's/6.log/06.log/g' | sed 's/7.log/07.log/g' | sed 's/8.log/08.log/g' | sed 's/9.log/09.log/g'`
		mv $i $name
	done

	create_subdirs;

	cd ..
}

echo `pwd`
cd $LOGS_DIR/1000_data/
organize_files
cd ../10000_data/
organize_files
cd ../100000_data/
organize_files
cd ../1000000_data/
organize_files
cd ..
