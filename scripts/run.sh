#!/bin/bash

MAX_NUMBER_OF_THREADS=10
MAX_NUMBER_OF_REPETITION=30
FAIR=true;
NOT_FAIR=false;
COMAND="java -jar fpc-project.jar";
LOGS_PATH=logs

rm -rf $LOGS_PATH
mkdir $LOGS_PATH

for num_operations in 1000 10000 100000 1000000; do
	# running with justice
	for teste_to_run in `seq 1 7`; do
		for num_threads in `seq 1 $MAX_NUMBER_OF_THREADS`; do
			for i in `seq 1 $MAX_NUMBER_OF_REPETITION`; do
				echo "Runnning with justice: num_operations=$num_operations, teste_to_run=$teste_to_run, num_threads=$num_threads, seq=$i"
				$COMAND $teste_to_run $FAIR $num_threads $num_operations >> $LOGS_PATH/com_justica-$num_operations-$teste_to_run-$num_threads.log
			done
		done
	done

	# without justice
	for teste_to_run in `seq 1 7`; do
		for num_threads in `seq 1 $MAX_NUMBER_OF_THREADS`; do
			for i in `seq 1 $MAX_NUMBER_OF_REPETITION`; do
				echo "Runnning no justice: num_operations=$num_operations, teste_to_run=$teste_to_run, num_threads=$num_threads, seq=$i"
				$COMAND $teste_to_run $NOT_FAIR $num_threads $num_operations >> $LOGS_PATH/sem_justica-$num_operations--$teste_to_run-$num_threads.log
			done
		done
	done
done
