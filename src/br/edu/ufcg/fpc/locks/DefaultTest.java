package br.edu.ufcg.fpc.locks;

import java.util.LinkedList;
import java.util.List;


public abstract class DefaultTest<T extends Thread> implements Testable {
	private final static String SEPARATOR = " ";
	
	protected final String experimentName;
	private final boolean isFair;
	private final int numThreads;
	private final int numTotalOperation;
	
	private final List<T> threads = new LinkedList<T>();
	
	public DefaultTest(String experimentName, boolean isFair, int numThreads, int numTotalOperation) {
		this.experimentName = experimentName;
		this.isFair = isFair;
		this.numThreads = numThreads;
		this.numTotalOperation = numTotalOperation;
	}
	
	public void runTest() {
		createThreads();
		long startTime = System.currentTimeMillis();
		startThreads();
		joinThreads();
		long finalTime = System.currentTimeMillis() - startTime;
		System.out.println(this.experimentName + SEPARATOR +
						this.isFair + SEPARATOR + 
						this.numThreads + SEPARATOR + 
						this.numTotalOperation + SEPARATOR + finalTime);
		printNumOfThreadOperations();
	}

	private void joinThreads() {
		for (T lockTest : threads) {
			try {
				lockTest.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void printNumOfThreadOperations() {
		for (T lockTest : threads) {
			System.out.println(lockTest.getName() + SEPARATOR +
			((TestableThread)lockTest).numOfThreadOperations());
		}
	}

	private void startThreads() {
		for (T lockTest : threads) {	
			lockTest.start();
		}
	}
	
	protected int totalOperations() {
		return this.numTotalOperation;
	}
	
	protected int numThreads() {
		return this.numThreads;
	}
	
	protected boolean isFair() {
		return this.isFair;
	}
	
	protected boolean addThread(T thread) {
		return this.threads.add(thread);
	}
		
	abstract protected void createThreads();
}
