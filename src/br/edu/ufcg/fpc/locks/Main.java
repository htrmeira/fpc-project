package br.edu.ufcg.fpc.locks;

import br.edu.ufcg.fpc.locks.arrayblockingqueue.ArrayBlockingQueueTest;
import br.edu.ufcg.fpc.locks.reentrantlock.ReentrantLockTest;
import br.edu.ufcg.fpc.locks.reentrantreadwriterlock.ReentrantReadWriteLockTest;
import br.edu.ufcg.fpc.locks.semaphore.SemaphoreTest;
import br.edu.ufcg.fpc.locks.synchronizedtest.SynchronizedTest;
import br.edu.ufcg.fpc.locks.synchronousqueue.SynchronousQueueTest;

public class Main {
	private static final int REENTRANT_LOCK_TEST = 1;
	private static final int SEMAPHORE_TEST = 2;
	private static final int REENTRANT_READ_WRITE_TEST = 3;
	private static final int SYNCHRONOUS_QUEUE_TEST = 4;
	private static final int ARRAY_BLOCKING_QUEUE_TEST = 5;
	private static final int ARRAY_BLOCKING_QUEUE_TEST_BLOCKING = 6;
	private static final int SYNCHRONIZED_TEST = 7;
	
	public static void main(String[] args) {
		printHelp(args);

		int testToRun = testeToRun(args);
		boolean isFair = isFair(args);
		int numThread = numThreads(args);
		int numOperation = numOperations(args);
		
		Testable test = teste(testToRun, isFair, numThread, numOperation);
		test.runTest();
	}
	
	private static int testeToRun(String[] args) {
		return Integer.parseInt(args[0]);
	}
	
	private static boolean isFair(String[] args) {
		return Boolean.parseBoolean(args[1]);
	}
	
	private static int numThreads(String[] args) {
		return Integer.parseInt(args[2]);
	}
	
	private static int numOperations(String[] args) {
		return Integer.parseInt(args[3]);
	}
	
	private static void printHelp(String[] args) {
		if(args.length != 4 || args[0].equalsIgnoreCase("help")) {
			System.out.println("args.length: " + args.length);
			for (String arg : args) {
				System.out.println("Arg=" + arg);
			}
			System.out.println("=================================");
			
			System.out.println("REENTRANT_LOCK_TEST: " + 1);
			System.out.println("SEMAPHORE_TEST: " + 2);
			System.out.println("REENTRANT_READ_WRITE_TEST: " + 3);
			System.out.println("SYNCHRONOUS_QUEUE_TEST: " + 4);
			System.out.println("ARRAY_BLOCKING_QUEUE_TEST: " + 5);
			System.out.println("ARRAY_BLOCKING_QUEUE_TEST_BLOCKING: " + 6);
			System.out.println("SYNCHRONIZED_TEST: " + 7);
			System.out.println("Use: java -jar experimentosfpc.jar testToRun isFair numThreads numOperations");
			System.exit(0);
		}
	}
	
	public static Testable teste(int testNumber, boolean isFair, int numThreads, int numOperations) {
		switch(testNumber) {
		case REENTRANT_LOCK_TEST:
			return new ReentrantLockTest(isFair, numThreads, numOperations);
		case SEMAPHORE_TEST:
			return new SemaphoreTest(isFair, numThreads, numOperations);
		case REENTRANT_READ_WRITE_TEST:
			return new ReentrantReadWriteLockTest(isFair, numThreads, numOperations);
		case SYNCHRONOUS_QUEUE_TEST:
			return new SynchronousQueueTest(isFair, numThreads, numOperations);
		case ARRAY_BLOCKING_QUEUE_TEST:
			return new ArrayBlockingQueueTest(isFair, false, numThreads, numOperations);
		case ARRAY_BLOCKING_QUEUE_TEST_BLOCKING:
			return new ArrayBlockingQueueTest(isFair, true, numThreads, numOperations);
		case SYNCHRONIZED_TEST:
			return new SynchronizedTest(numThreads, numOperations);
		default:
			return null;
		}
	}
}
