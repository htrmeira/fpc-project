package br.edu.ufcg.fpc.locks;

public interface Testable {
	void runTest();
}
