package br.edu.ufcg.fpc.locks;

public interface TestableThread {
	int numOfThreadOperations();
}
