package br.edu.ufcg.fpc.locks.arrayblockingqueue;

import java.util.concurrent.ArrayBlockingQueue;

import br.edu.ufcg.fpc.locks.DefaultTest;

public class ArrayBlockingQueueTest extends DefaultTest<ArrayBlockingQueueThread> {
	private final static String EXPERIMENT_NAME = "[ArrayBlockingQueueTest]";
	private static final int MAX_AVAILABLE = 1000;
	private final boolean isBlocking;
	
	public ArrayBlockingQueueTest(boolean isFair, boolean isBlocking, int numThreads, int numTotalOperation) {
		super(EXPERIMENT_NAME, isFair, numThreads, numTotalOperation);
		this.isBlocking = isBlocking;
	}
	
	@Override
	protected void createThreads() {
		ArrayBlockingQueue<Integer> array = new ArrayBlockingQueue<Integer>(MAX_AVAILABLE, isFair());
		for (int i = 0; i < super.numThreads(); i++) {
			super.addThread(new ArrayBlockingQueueThread(array, "Thread" + i, super.totalOperations()/super.numThreads(), this.isBlocking));
		}
	}
}