package br.edu.ufcg.fpc.locks.arrayblockingqueue;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;

import br.edu.ufcg.fpc.locks.TestableThread;

public class ArrayBlockingQueueThread extends Thread implements TestableThread {
	private int numOperationsOfThisThread = 0;
	private final ArrayBlockingQueue<Integer> array;
	private final int numOperations;
	private final boolean isBlocking;
	private Random numGenerator = new Random();
	
	public ArrayBlockingQueueThread(ArrayBlockingQueue<Integer> array, String threadName, int numOperations, boolean isBlocking) {
		setName(threadName);
		this.array = array;
		this.numOperations = numOperations;
		this.isBlocking = isBlocking;
	}
	
	@Override
	public void run() {
		while (++this.numOperationsOfThisThread < this.numOperations) {			
			if(numGenerator.nextInt(200) < 100) {
				if(this.array.remainingCapacity() >= this.array.size() - 1) {
					runProducer();
				} else {
					runConsumer();
				}
			} else {
				if(this.array.remainingCapacity() <= 1) {
					runConsumer();
				} else {
					runProducer();
				}
			}
		}
	}

	private void runConsumer() {
		try {
			if(this.isBlocking) {
				this.array.take();
			} else {
				this.array.poll();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private void runProducer() {
		try {
			if(this.isBlocking) {
				this.array.put(0);
			} else {
				this.array.offer(0);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public int numOfThreadOperations() {
		return this.numOperationsOfThisThread;
	}
	
	public boolean isBlocking() {
		return this.isBlocking;
	}
}
