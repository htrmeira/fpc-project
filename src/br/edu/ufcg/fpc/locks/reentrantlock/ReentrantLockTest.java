package br.edu.ufcg.fpc.locks.reentrantlock;

import java.util.concurrent.locks.ReentrantLock;

import br.edu.ufcg.fpc.locks.DefaultTest;

public class ReentrantLockTest extends DefaultTest<ReentrantLockThread> {
	private final static String EXPERIMENT_NAME = "[ReentrantLockTest]";
	
	public ReentrantLockTest(boolean isFair, int numThreads, int numTotalOperation) {
		super(EXPERIMENT_NAME, isFair, numThreads, numTotalOperation);
	}
	
	@Override
	protected void createThreads() {
		ReentrantLock lock = new ReentrantLock(super.isFair());
		for (int i = 0; i < super.numThreads(); i++) {
			super.addThread(new ReentrantLockThread("Thread" + i, super.totalOperations(), lock));
		}
	}
}
