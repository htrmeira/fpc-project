package br.edu.ufcg.fpc.locks.reentrantlock;

import java.util.concurrent.locks.ReentrantLock;

import br.edu.ufcg.fpc.locks.TestableThread;

public class ReentrantLockThread extends Thread implements TestableThread {
	private static int numRealizedOperations = 0;
	private int numOperationsOfThisThread = 0;
	private final ReentrantLock lock;
	private final int numOperations;
	
	public ReentrantLockThread(String threadName, int numOperations, ReentrantLock lock) {
		setName(threadName);
		this.lock = lock;
		this.numOperations = numOperations;
	}

	@Override
	public void run() {
		while (numRealizedOperations < this.numOperations) {
			runTests();
		}
	}
	
	private void runTests() {
		lock.lock();
		numRealizedOperations++;
		this.numOperationsOfThisThread++;
		lock.unlock();
		
	}

	@Override
	public int numOfThreadOperations() {
		return this.numOperationsOfThisThread;
	}
}
