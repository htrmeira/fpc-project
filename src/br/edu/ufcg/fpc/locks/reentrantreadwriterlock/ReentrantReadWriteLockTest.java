package br.edu.ufcg.fpc.locks.reentrantreadwriterlock;

import java.util.concurrent.locks.ReentrantReadWriteLock;

import br.edu.ufcg.fpc.locks.DefaultTest;

public class ReentrantReadWriteLockTest extends DefaultTest<ReentrantReadWriteLockThread> {
	private final static String EXPERIMENT_NAME = "[ReentrantReadWriteLockTest]";
	
	public ReentrantReadWriteLockTest(boolean isFair, int numThreads, int numTotalOperation) {
		super(EXPERIMENT_NAME, isFair, numThreads, numTotalOperation);
	}
	
	@Override
	protected void createThreads() {
		ReentrantReadWriteLock lock = new ReentrantReadWriteLock(super.isFair());
		for (int i = 0; i < super.numThreads(); i++) {
			super.addThread(new ReentrantReadWriteLockThread("Thread" + i, super.totalOperations()/super.numThreads(), lock));
		}
	}
}