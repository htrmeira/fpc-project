package br.edu.ufcg.fpc.locks.reentrantreadwriterlock;

import java.util.Random;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import br.edu.ufcg.fpc.locks.TestableThread;

public class ReentrantReadWriteLockThread extends Thread implements TestableThread {
	private int numOperationsOfThisThread = 0;
	private final ReentrantReadWriteLock lock;
	private final int numOperations;
	private Random numGenerator = new Random();
	
	public ReentrantReadWriteLockThread(String threadName, int numOperations, ReentrantReadWriteLock lock) {
		setName(threadName);
		this.lock = lock;
		this.numOperations = numOperations;
	}
	
	@Override
	public void run() {
		while (++numOperationsOfThisThread < this.numOperations) {
			if(numGenerator.nextInt(200) < 100) {
				if(this.lock.writeLock().tryLock()) { 
					runWriter();
				}
			} else {
				if(this.lock.readLock().tryLock()) { 
					runReader();
				}
			}
		}
	}
	
	private void runReader() {
		this.lock.readLock().unlock();
	}
	
	private void runWriter() {
		this.lock.writeLock().unlock();
	}

	@Override
	public int numOfThreadOperations() {
		return this.numOperationsOfThisThread;
	}
}
