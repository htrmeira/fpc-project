package br.edu.ufcg.fpc.locks.semaphore;

import java.util.concurrent.Semaphore;

import br.edu.ufcg.fpc.locks.DefaultTest;

public class SemaphoreTest extends DefaultTest<SemaphoreThread> {
	private static final int MAX_AVAILABLE = 1;
	private final static String EXPERIMENT_NAME = "[SemaphoreTest]";
	
	public SemaphoreTest(boolean isFair, int numThreads, int numTotalOperation) {
		super(EXPERIMENT_NAME, isFair, numThreads, numTotalOperation);
	}
	
	@Override
	protected void createThreads() {
		Semaphore semaphore = new Semaphore(MAX_AVAILABLE, super.isFair());
		for (int i = 0; i < super.numThreads(); i++) {
			super.addThread(new SemaphoreThread("Thread" + i, super.totalOperations(), semaphore));
		}
	}
}
