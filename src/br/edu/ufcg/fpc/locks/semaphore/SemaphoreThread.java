package br.edu.ufcg.fpc.locks.semaphore;

import java.util.concurrent.Semaphore;

import br.edu.ufcg.fpc.locks.TestableThread;

public class SemaphoreThread extends Thread implements TestableThread {
	private static int numRealizedOperations = 0;
	private int numOperationsOfThisThread = 0;
	private final Semaphore semaphore;
	private final int numOperations;
	
	
	public SemaphoreThread(String threadName, int numOperations, Semaphore semaphore) {
		this.semaphore = semaphore;
		 this.numOperations = numOperations;
	}
	
	@Override
	public void run() {
		while (numRealizedOperations < this.numOperations) {
			runTests();
		}
	}
	
	private void runTests() {
		try {
			this.semaphore.acquire();
			numRealizedOperations++;
			this.numOperationsOfThisThread++;
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.semaphore.release();
		}
	}

	@Override
	public int numOfThreadOperations() {
		return this.numOperationsOfThisThread;
	}
}
