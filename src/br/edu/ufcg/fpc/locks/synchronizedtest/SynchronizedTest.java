package br.edu.ufcg.fpc.locks.synchronizedtest;

import br.edu.ufcg.fpc.locks.DefaultTest;

public class SynchronizedTest extends DefaultTest<SynchronizedThread> {
	private final static String EXPERIMENT_NAME = "[SynchronizedTest]";
	
	public SynchronizedTest(int numThreads, int numTotalOperation) {
		super(EXPERIMENT_NAME, false, numThreads, numTotalOperation);
	}
	
	@Override
	protected void createThreads() {
		for (int i = 0; i < super.numThreads(); i++) {
			super.addThread(new SynchronizedThread("Thread" + i, super.totalOperations()));
		}
	}
}