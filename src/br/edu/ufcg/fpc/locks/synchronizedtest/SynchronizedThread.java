package br.edu.ufcg.fpc.locks.synchronizedtest;

import br.edu.ufcg.fpc.locks.TestableThread;

public class SynchronizedThread extends Thread implements TestableThread {
	private static int numRealizedOperations = 0;
	private int numOperationsOfThisThread = 0;
	private final int numOperations;
	
	public SynchronizedThread(String threadName, int numOperations) {
		setName(threadName);
		this.numOperations = numOperations;
	}
	
	@Override
	public void run() {
		while (incrementNumOperations() < this.numOperations) {
			this.numOperationsOfThisThread++;
		}
	}
	
	private static synchronized int incrementNumOperations() {
		return numRealizedOperations++;
	}

	@Override
	public int numOfThreadOperations() {
		return this.numOperationsOfThisThread;
	}
}
