package br.edu.ufcg.fpc.locks.synchronousqueue;

import java.util.concurrent.SynchronousQueue;

import br.edu.ufcg.fpc.locks.DefaultTest;

public class SynchronousQueueTest extends DefaultTest<SynchronousQueueThread> {
	private final static String EXPERIMENT_NAME = "[SynchronousQueueTest]";
	
	public SynchronousQueueTest(boolean isFair, int numThreads, int numTotalOperation) {
		super(EXPERIMENT_NAME, isFair, numThreads, numTotalOperation);
	}
	
	@Override
	protected void createThreads() {
		SynchronousQueue<Integer> array = new SynchronousQueue<Integer>(super.isFair());
		for (int i = 0; i < super.numThreads(); i++) {
			super.addThread(new SynchronousQueueThread(array, "Thread" + i, super.totalOperations()/super.numThreads() ));
		}
	}
}