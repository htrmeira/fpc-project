package br.edu.ufcg.fpc.locks.synchronousqueue;

import java.util.Random;
import java.util.concurrent.SynchronousQueue;

import br.edu.ufcg.fpc.locks.TestableThread;

public class SynchronousQueueThread extends Thread implements TestableThread {
	private int numOperationsOfThisThread = 0;
	private final SynchronousQueue<Integer> array;
	private final int numOperations;
	private Random numGenerator = new Random();
	
	public SynchronousQueueThread(SynchronousQueue<Integer> array, String threadName, int numOperations) {
		setName(threadName);
		this.array = array;
		this.numOperations = numOperations;
	}
	
	@Override
	public void run() {
		while (++this.numOperationsOfThisThread < this.numOperations) {
			if(numGenerator.nextInt(200) < 100) {
				runConsumer();
			} else {
				runProducer();
			}
		}
	}

	private void runConsumer() {
		this.array.poll();
	}
	
	private void runProducer() {
		this.array.offer(0);
	}

	@Override
	public int numOfThreadOperations() {
		return this.numOperationsOfThisThread;
	}
}
